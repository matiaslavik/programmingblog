+++
title = "About"
date = 2022-01-01
author = "Matias Lavik"
description = "Things about me."
+++

## About

I'm a Norwegian software developer. I mostly work with C++ and C#, and my favourite work areas are game engines, computer graphics, rendering and visualisation.

I store most of my code on [Codeberg](https://codeberg.org/matiaslavik) and [GitHub](https://github.com/mlavik1).

