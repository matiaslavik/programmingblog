+++
title = "Contact"
date = 2022-01-01
author = "Matias Lavik"
description = "How to contact me."
+++

## Contact

For questions about my GitHub/Codeberg repositories, I appreciate if you create an issue there. I check those more often than I check my social media accounts and even e-mail.

You can also find me on the following social meda platforms:
- [Personal Mastodon account](https://floss.social/@sigsegv)
- [UnityVolumeRendering on Mastodon](https://fosstodon.org/web/@unityvolrend)
